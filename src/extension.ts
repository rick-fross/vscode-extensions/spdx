/*
 * SPDX-PackageName: SPDX
 * SPDX-PackageVersion: 0.1.0
 * SPDX-FileCopyrightText: © 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "spdx" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('spdx.helloWorld', () => {
        // The code you place here will be executed every time your command is executed
        // Display a message box to the user
        vscode.window.showInformationMessage('Hello World from SPDX!');
    });

    const provider = vscode.languages.registerCompletionItemProvider(
        [
            'plaintext',
            {scheme: "file", language: "typescript"},
            {scheme: "file", language: "javascript"}
        ],
        {

        provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {

            // a simple completion item which inserts `Hello World!`
            const simpleApacheLicenseIdentifierCompletion = new vscode.CompletionItem('SPDX-License-Identifier: Apache-2.0' );
            const simpleMITLicenseIdentifierCompletion = new vscode.CompletionItem('SPDX-License-Identifier: MIT' );

            // a completion item that inserts its text as snippet,
            // the `insertText`-property is a `SnippetString` which will be
            // honored by the editor.
            const currentYear = new Date().getFullYear();
            const snippetCompletion = new vscode.CompletionItem('SPDX Minimal File Header');
            snippetCompletion.insertText = new vscode.SnippetString(
                `/**\n` +
                ' * SPDX-License-Identifier: ${1|MIT,Apache-2.0|}\n' +
                ` *\n` +
                ' * SPDX-FileCopyrightText: Copyright © ${2|' + currentYear + ',-' + currentYear + '|} Your Name <your.email@domain.ext>\n' +
                ` */\n` +
                `\n`
            );
            const docs : any = new vscode.MarkdownString("Inserts a snippet that lets you select [link](x.ts).");
            snippetCompletion.documentation = docs;
            docs.baseUri = vscode.Uri.parse('https://spdx.dev/');

            // a completion item that can be accepted by a commit character,
            // the `commitCharacters`-property is set which means that the completion will
            // be inserted and then the character will be typed.
            const commitCharacterCompletion = new vscode.CompletionItem('SPDX');
            commitCharacterCompletion.commitCharacters = ['-'];
            commitCharacterCompletion.documentation = new vscode.MarkdownString('Press `-` to get `SPDX.`');

            const commitCharacterCompletion2 = new vscode.CompletionItem('SPDXID');
            commitCharacterCompletion2.commitCharacters = [':'];
            commitCharacterCompletion2.documentation = new vscode.MarkdownString('Press `:` to get `SPDXID.`');

            const commitCharacterCompletion3 = new vscode.CompletionItem('SPDX-FileType');
            commitCharacterCompletion3.commitCharacters = [':'];
            commitCharacterCompletion3.documentation = new vscode.MarkdownString('Press `:` to get `SPDX-FileType.`');

            const commitCharacterCompletion4 = new vscode.CompletionItem('SnippetSPDXID');
            commitCharacterCompletion4.commitCharacters = [':'];
            commitCharacterCompletion4.documentation = new vscode.MarkdownString('Press `:` to get `SnippetSPDXID.`');

            const commitCharacterCompletion5 = new vscode.CompletionItem('SPDX-LicenseID');
            commitCharacterCompletion5.commitCharacters = [':'];
            commitCharacterCompletion5.documentation = new vscode.MarkdownString('Press `:` to get `SPDX-LicenseID.`');

            // a completion item that retriggers IntelliSense when being accepted,
            // the `command`-property is set which the editor will execute after
            // completion has been inserted. Also, the `insertText` is set so that
            // a space is inserted after `new`
            //const commandCompletion = new vscode.CompletionItem('new');
            //commandCompletion.kind = vscode.CompletionItemKind.Keyword;
            //commandCompletion.insertText = 'new ';
            //commandCompletion.command = { command: 'editor.action.triggerSuggest', title: 'Re-trigger completions...' };

            // return all completion items as array
            return [
                simpleApacheLicenseIdentifierCompletion,
                simpleMITLicenseIdentifierCompletion,
                snippetCompletion,
                commitCharacterCompletion,
                commitCharacterCompletion2,
                commitCharacterCompletion3,
                commitCharacterCompletion4,
                commitCharacterCompletion5//,
                //commandCompletion
            ];
        }
    });


    const spdxSpecialTagsProvider = vscode.languages.registerCompletionItemProvider(
        [
            'plaintext',
            {scheme: "file", language: "typescript"},
            {scheme: "file", language: "javascript"}
        ],
        {
            provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {

                // get all text until the `position` and check if it reads `console.`
                // and if so then complete if `log`, `warn`, and `error`
                const linePrefix = document.lineAt(position).text.substr(0, position.character);
                if (!linePrefix.endsWith('SPDXID: ')) {
                    return undefined;
                }

                return [
                    //new vscode.CompletionItem('ID: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem(' SPDXRef-', vscode.CompletionItemKind.Snippet),
                    //new vscode.CompletionItem('Version: ', vscode.CompletionItemKind.Snippet),
                ];
            }
        },
        ' ' // triggered whenever a ' ' is being typed
    );


    const spdxFileTypeProvider = vscode.languages.registerCompletionItemProvider(
        [
            'plaintext',
            {scheme: "file", language: "typescript"},
            {scheme: "file", language: "javascript"}
        ],
        {
            provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {

                // get all text until the `position` and check if it reads `console.`
                // and if so then complete if `log`, `warn`, and `error`
                const linePrefix = document.lineAt(position).text.substr(0, position.character);
                if (!linePrefix.endsWith('SPDX-FileType: ')) {
                    return undefined;
                }

                return [
                    new vscode.CompletionItem('SOURCE', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('BINARY', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('ARCHIVE', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('APPLICATION', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('AUDIO', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('IMAGE', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('TEXT', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('VIDEO', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('DOCUMENTATION', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SPDX', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('OTHER', vscode.CompletionItemKind.Snippet),
                ];
            }
        },
        ' ' // triggered whenever a ' ' is being typed
    );


    const spdxSnippetTagsProvider = vscode.languages.registerCompletionItemProvider(
        [
            'plaintext',
            {scheme: "file", language: "typescript"},
            {scheme: "file", language: "javascript"}
        ],
        {
            provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {

                // get all text until the `position` and check if it reads `console.`
                // and if so then complete if `log`, `warn`, and `error`
                const linePrefix = document.lineAt(position).text.substr(0, position.character);
                if (!linePrefix.endsWith('SnippetSPDXID: ')) {
                    return undefined;
                }

                return [
                    new vscode.CompletionItem(' SPDXRef-', vscode.CompletionItemKind.Snippet)
                ];
            }
        },
        ' ' // triggered whenever a 'X' is being typed
    );


    const spdxLicenseTagProvider = vscode.languages.registerCompletionItemProvider(
        [
            'plaintext',
            {scheme: "file", language: "typescript"},
            {scheme: "file", language: "javascript"}
        ],
        {
            provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {

                // get all text until the `position` and check if it reads `console.`
                // and if so then complete if `log`, `warn`, and `error`
                const linePrefix = document.lineAt(position).text.substr(0, position.character);
                if (!linePrefix.endsWith('SPDX-LicenseID: ')) {
                    return undefined;
                }

                return [
                    //new vscode.CompletionItem('ID: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem(' SPDXRef-', vscode.CompletionItemKind.Snippet),
                    //new vscode.CompletionItem('Name: ', vscode.CompletionItemKind.Snippet),
                    //new vscode.CompletionItem('CrossReference: ', vscode.CompletionItemKind.Snippet),
                    //new vscode.CompletionItem('Comment: ', vscode.CompletionItemKind.Snippet)
                ];
            }
        },
        ' ' // triggered whenever a ' ' is being typed
    );


    const spdxTagProvider = vscode.languages.registerCompletionItemProvider(
        [
            'plaintext',
            {scheme: "file", language: "typescript"},
            {scheme: "file", language: "javascript"}
        ],
        {
            provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {

                // get all text until the `position` and check if it reads `console.`
                // and if so then complete if `log`, `warn`, and `error`
                const linePrefix = document.lineAt(position).text.substr(0, position.character);
                if (!linePrefix.endsWith('SPDX-')) {
                    return undefined;
                }

                return [
                    new vscode.CompletionItem('DataLicense: ', vscode.CompletionItemKind.Snippet),                   // DOCUMENT
                    new vscode.CompletionItem('DocumentName: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('DocumentNamespace: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('ExternalDocumentRef: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('LicenseListVersion: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('Creator: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('Created: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('CreatorComment: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('DocumentComment: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageName: ', vscode.CompletionItemKind.Snippet),                   // PACKAGE
                    new vscode.CompletionItem('PackageVersion: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageFileName: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageSupplier: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageOriginator: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageDownloadLocation: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FilesAnalyzed: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageVerificationCode: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageChecksum: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageSourceInfo: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageLicenseConcluded: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageLicenseInfoFromFiles: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageLicenseDeclared: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageLicenseComments: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageCopyrightText: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageSummary: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageDescription: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageComment: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('ExternalRef: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('ExternalRefComment: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('PackageAttributionText: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileName: ', vscode.CompletionItemKind.Snippet),                      // FILES
                    new vscode.CompletionItem('FileType: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileChecksum: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('LicenseConcluded: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('LicenseInfoInFile: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('LicenseComments: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileCopyrightText: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('ArtifactOfProjectName: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('ArtifactOfProjectHomePage: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('ArtifactOfProjectURI: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileComment: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileNotice: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileContributor: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileAttributionText: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileDependency: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('FileNotice: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetFromFileSPDXID: ', vscode.CompletionItemKind.Snippet),        // SNIPPET
                    new vscode.CompletionItem('SnippetByteRange: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetLineRange: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetLicenseConcluded: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('LicenseInfoInSnippet: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetLicenseComments: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetCopyrightText: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetComment: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetName: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('SnippetAttributionText: ', vscode.CompletionItemKind.Snippet),
                    new vscode.CompletionItem('License', vscode.CompletionItemKind.Snippet),                        // OTHER
                    new vscode.CompletionItem('ExtractedText', vscode.CompletionItemKind.Snippet)
                ];
            }
        },
        '-' // triggered whenever a '.' is being typed
    );

    context.subscriptions.push(disposable, provider, spdxTagProvider, spdxFileTypeProvider, spdxSpecialTagsProvider, spdxSnippetTagsProvider, spdxLicenseTagProvider);
}



// this method is called when your extension is deactivated
export function deactivate() {}
