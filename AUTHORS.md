# Kwaeri Module Template Authors

This file lists the authors of - and contributors to - this project.

## The List

| From |      |
|------|------|
| 2014 | [Richard Winters](mailto:kirvedx@gmail.com) |