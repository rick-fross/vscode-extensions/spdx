# LICENSE

Copyright 2022 Richard Winters <kirvedx@gmail.com> and contributors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Copying

Complete copyright information can be found in the `copyright` file
located under the `DEP5` directory within the root of this repository.

Please review the full license text at the link specified above in the
brief, or in the respective file located under the `LICENSES` directory
within the  root of this repository.

To review the list of contributors, see the `AUTHORS.md` file in the
root of this repository.
